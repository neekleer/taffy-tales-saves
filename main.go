package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"strings"

	"golang.org/x/sys/windows/registry"
)

func checkTarget(validTargets []string, target string) bool {
	if target == "" {
		fmt.Println(`Specify name to export e.g. export "wholeGameStateversion 0.11.2c_h3390797566"`)
		return false
	}

	for i := range validTargets {
		if validTargets[i] == target {
			return true
		}
	}

	fmt.Printf("Save data not present for %s\n", target)
	return false
}

func exportSave(ttKey registry.Key, target string, suffix string, onlyIfNew bool) {
	exportFileName := fmt.Sprintf("%s.json%s", target, suffix)
	if onlyIfNew {
		if _, err := os.Stat(exportFileName); err == nil {
			return
		}
	}

	ttRegBytes, _, err := ttKey.GetBinaryValue(target)
	if err != nil {
		fmt.Println("Save data not accessible")
	}
	ttRegBytes = ttRegBytes[:len(ttRegBytes)-1]

	var bytesForFile bytes.Buffer
	json.Indent(&bytesForFile, ttRegBytes, "", "    ")
	ttRegBytes = nil

	f, err := os.Create(exportFileName)
	if err != nil {
		fmt.Println("Export file could not be created")
		return
	}
	defer f.Close()

	bfWriter := bufio.NewWriter(f)
	defer bfWriter.Flush()
	_, err = bytesForFile.WriteTo(bfWriter)
	if err != nil {
		fmt.Printf("%v\n", err)
		fmt.Println("Export file could not be created")
		return
	}
}

func importSave(regPath string, target string) {
	exportFileName := fmt.Sprintf("%s.json", target)
	f, err := os.Open(exportFileName)
	if err != nil {
		fmt.Println("JSON file not present")
		return
	}
	defer f.Close()

	bfReader := bufio.NewReader(f)

	var jsonRaw json.RawMessage
	dec := json.NewDecoder(bfReader)
	for {
		if err := dec.Decode(&jsonRaw); err == io.EOF {
			break
		} else if err != nil {
			fmt.Println("JSON file unparseable")
			return
		}
	}
	f.Close()

	bytesForReg, err := json.Marshal(&jsonRaw)
	if err != nil {
		fmt.Println("JSON file unparseable")
		return
	}
	bytesForReg = append(bytesForReg, 0)

	ttKey, err := registry.OpenKey(registry.CURRENT_USER, regPath, registry.SET_VALUE)
	if err != nil {
		fmt.Println("Save data not acccesible")
	}

	ttKey.SetBinaryValue(target, bytesForReg)
}

func main() {
	action := "names"
	if len(os.Args) > 1 {
		action = os.Args[1]
	}
	target := ""
	if len(os.Args) > 2 {
		target = os.Args[2]
	}

	if target == "wholeGameStateversion" && len(os.Args) > 3 {
		target = fmt.Sprintf("%s %s", target, os.Args[3])
	}

	regPath := `Software\UberPie\TaffyTales`
	ttKey, err := registry.OpenKey(registry.CURRENT_USER, regPath, registry.QUERY_VALUE)
	if err != nil {
		fmt.Println("Save data not present")
		return
	}
	defer ttKey.Close()

	ttValueNames, err := ttKey.ReadValueNames(0)
	if err != nil {
		fmt.Println("Save data not present")
		return
	}

	var validTargets []string
	for i := range ttValueNames {
		if strings.HasPrefix(ttValueNames[i], "wholeGameStateversion ") {
			validTargets = append(validTargets, ttValueNames[i])
		}
	}

	switch action {
	case "names":
		for i := range validTargets {
			fmt.Println(validTargets[i])
		}
	case "export":
		if checkTarget(validTargets, target) {
			exportSave(ttKey, target, "", false)
		}
	case "import":
		if checkTarget(validTargets, target) {
			exportSave(ttKey, target, ".old", true)
			importSave(regPath, target)
		}
	}
}
